/**
 * Created with IntelliJ IDEA.
 * User: baus
 * Date: 12/16/12
 * Time: 10:27 PM
 * To change this template use File | Settings | File Templates.
 */
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public class MyLossyChecker implements LossyChecker {

    @Override
    public boolean isLossy(String s, String encoding) throws UnsupportedEncodingException
    {
        Charset charset = null;
        try{
            charset = Charset.forName(encoding);
        }
        catch(Exception e){
            throw new UnsupportedEncodingException();
        }
        CharsetEncoder encoder = charset.newEncoder();
        for(int i = 0; i < s.length(); ++i){
            if(!encoder.canEncode(s.charAt(i))){
                return true;
            }
        }
        return false;
    }
}
