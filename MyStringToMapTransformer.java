
import java.io.UnsupportedEncodingException;
import java.lang.Integer;
import java.lang.String;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class MyStringToMapTransformer implements StringToMapTransformer {
    @Override
    public Map<String, Integer> parseNumbers(String csvString) {
        //break comma separated line using ","
        HashMap<String, Integer> returnHashMap = new HashMap<String, Integer>();
        StringTokenizer st = new StringTokenizer(csvString, ",");
        while(st.hasMoreTokens())
        {
            StringTokenizer mapTokenizer = new StringTokenizer(st.nextToken(), "->");
            if(mapTokenizer.hasMoreTokens())
            {
                String key = mapTokenizer.nextToken();
                if(mapTokenizer.hasMoreTokens()){
                    returnHashMap.put(key, Integer.parseInt(mapTokenizer.nextToken()));
                }
            }

        }
        return returnHashMap;
    }


    public static void main(String[] args)
    {
        MyStringToMapTransformer transformer = new MyStringToMapTransformer();
        transformer.parseNumbers("Mary Dimple->97");
        transformer.parseNumbers(" A crazy name!!!ABC  ->24");
        transformer.parseNumbers("Name 1->10,Name 2->20");
        MyMapDecoder myMapDecoder = new MyMapDecoder();
        Map<String, String> map=null;

        map=myMapDecoder.decode("a=1&b=2");
        map=myMapDecoder.decode("");
        map=myMapDecoder.decode(null);
        try{
            map=myMapDecoder.decode("asdlfjkskjlw");
        }catch(IllegalArgumentException e)
        {
            System.out.println(e);
        }
        map=myMapDecoder.decode("a=&=2");
        try{
            map=myMapDecoder.decode("a=b=c&b=2");
        }
        catch(IllegalArgumentException e)
        {
            System.out.println(e);
        }
        try{
            map=myMapDecoder.decode("a=b&&b=2");
        }
        catch(IllegalArgumentException e)
        {
            System.out.println(e);
        }
        MyLossyChecker myLossyChecker = new MyLossyChecker();
        try{
            boolean b = myLossyChecker.isLossy("abc", "UTF-8");
            System.out.println(b?"true":"false");
            b = myLossyChecker.isLossy("\u00A3", "US-ASCII");
            System.out.println(b?"true":"false");
            b = myLossyChecker.isLossy("\u00A3", "foobar");
            System.out.println(b?"true":"false");
        }
        catch(UnsupportedEncodingException e)
        {
            System.out.println(e);
        }
        MyFindCommonAncestor myFindCommonAncestor = new MyFindCommonAncestor();
        String[] commits = {"G", "F", "E", "D", "C", "B", "A"};
        String[][] parents ={{"F","D"},{"E"}, {"B"}, {"C"}, {"B"}, {"A"}, null};
        String commit1 = "C";
        String commit2 = "G";
        System.out.println(myFindCommonAncestor.findCommmonAncestor(commits, parents, commit1, commit2));
    }
}
