import java.util.HashMap;
import java.util.HashSet;
/**
 * Created with IntelliJ IDEA.
 * User: baus
 * Date: 12/16/12
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyFindCommonAncestor implements FindCommonAncestor
{
    class Node{
        Node(String id, int order, String Parent1, String Parent2){
            this.id = id;
            this.order = order;
            this.Parent1 = Parent1;
            this.Parent2 = Parent2;
        }
        public String id;
        public int order;
        public String Parent1;
        public String Parent2;

    }

    public String findCommmonAncestor(String[] commitHashes,
                                      String[][] parentHashes, String commitHash1, String commitHash2)
    {
        HashMap<String, Node> nodes = new HashMap<String, Node>();
        for(int i = 0; i < commitHashes.length; ++i){
            String[] parents = parentHashes[i];
            Node n = null;
            if(parents == null){
                n = new Node(commitHashes[i], i, null, null);
            }
            else if(parents.length == 1)
            {
                n = new Node(commitHashes[i], i, parents[0], null);
            }
            else if(parents.length == 2)
            {
                n = new Node(commitHashes[i], i, parents[0], parents[1]);
            }
            nodes.put(commitHashes[i], n);
        }
        HashSet<String> hash1Parents = new HashSet<String>();
        HashSet<String> hash2Parents = new HashSet<String>();
        findParents(nodes, commitHash1, hash1Parents);
        findParents(nodes, commitHash2, hash2Parents);
        hash1Parents.retainAll(hash2Parents);
        for(int i = 0; i < commitHashes.length; ++i){
            if(hash1Parents.contains(commitHashes[i]))
            {
                return commitHashes[i];
            }
        }
        return "";
    }

    public void findParents(HashMap<String, Node> nodes, String node, HashSet<String> output)
    {
        Node n = nodes.get(node);
        output.add(n.id);
        if(n.Parent1 != null)
        {
            findParents(nodes, n.Parent1, output);
        }
        if(n.Parent2 != null)
        {
            findParents(nodes, n.Parent2, output);
        }
    }
}
