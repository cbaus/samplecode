/**
 * Created with IntelliJ IDEA.
 * User: baus
 * Date: 12/16/12
 * Time: 9:42 PM
 * To change this template use File | Settings | File Templates.
 */
    import java.util.StringTokenizer;
    import java.util.Map;
    import java.util.HashMap;

    public class MyMapDecoder implements MapDecoder
    {
        @Override
        public Map<String, String> decode(String s)
        {
            if(s == null){
                return null;
            }
            Map<String, String> returnMap = new HashMap<String, String>();
            StringTokenizer st = new StringTokenizer(s, "&");
            while(st.hasMoreTokens())
            {
                String token = st.nextToken();
                StringTokenizer mapTokenizer = new StringTokenizer(token, "=");
                if(mapTokenizer.hasMoreTokens())
                {
                    String key = mapTokenizer.nextToken();
                    if(mapTokenizer.hasMoreTokens()){
                        returnMap.put(key, mapTokenizer.nextToken());
                    }
                    else{
                        if(token.startsWith("=")){
                            returnMap.put("", key);
                        }
                        else if (token.endsWith("=")){
                            returnMap.put(key,"");
                        }
                        else{
                            throw new IllegalArgumentException();
                        }
                    }
                    if(mapTokenizer.hasMoreTokens()){
                        throw new IllegalArgumentException();
                    }
                }
                else{
                    throw new IllegalArgumentException();
                }
            }
            if (returnMap.isEmpty() && !s.isEmpty())
            {
                throw new IllegalArgumentException();
            }
            return returnMap;
        }
    }

